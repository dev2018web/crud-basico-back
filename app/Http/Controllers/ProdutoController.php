<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();

        return response()->json(['produtos' => $produtos], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'nome' => 'required|max:255',
                'preco_compra' => 'required'
            ]);

            if($validator->fails()){
                $response = response()->json(['error' => $validator->errors()], 400);
            }else{
                extract($request->post());
                
                $data = [
                    'nome' => $nome,
                    'preco_compra' => $preco_compra,
                    'data_entrada' =>date("Y-m-d"),
                ];

                $produto = produto::create($data);

                $response = response()->json(['produto' => $produto], 202);
            }
        
            return $response;
        }catch(Exception $ex){
            return response()->json(['error' => $ex], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try{
           $produto = Produto::find($id);

            return response()->json(['produto' => $produto], 200);
        }catch(Exception $ex){
            return response()->json(['error' => $ex], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try{
            $validator = Validator::make($request->all(), [
                'nome' => 'required|max:255',
                'preco_compra' => 'required'
            ]);

            if($validator->fails()){
                $response = response()->json(['error' => $validator->errors()], 400);
            }else{
                extract($request->post());

                $data = [
                    'nome' => $nome,
                    'preco_compra' => $preco_compra
                ];

                $produto = Produto::find($id);

                if($produto){
                    $produto->update($data);
                }

                $response = response()->json(['produto' => $produto], 202);
            }
        
            return $response;
        }catch(Exception $ex){
            return response()->json(['error' => $ex], 500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto, $id)
    {
        try{
            $produto = Produto::find($id);
            $produto->delete();
 
             return response()->json(['deleted' => true], 200);
         }catch(Exception $ex){
             return response()->json(['error' => $ex], 500);
         }
    }
}
