<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    protected $table = 'clientes';

    use softDeletes;

    protected $fillable = [
        'nome',
        'cpf',
        'endereco',
        'telefone'
    ];
    
}
