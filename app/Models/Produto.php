<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produto extends Model
{
    protected $table = 'produtos';
    use SoftDeletes;
    
    protected $fillable = [
        'nome',
        'preco_compra',
        'data_entrada'
    ];
    
}
