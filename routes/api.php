<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


$router->group(['prefix' => '/cliente'], function() use ($router) {
    $router->get('', ['uses' => 'ClienteController@index']);
    $router->get('/{user_id}', ['uses' => 'ClienteController@show']);
    $router->post('', ['uses' => 'ClienteController@create']);
    $router->put('/{user_id}', ['uses' => 'ClienteController@edit']);
    $router->delete('/{user_id}', ['uses' => 'ClienteController@destroy']);
});

$router->group(['prefix' => '/produto'], function() use ($router) {
    $router->get('', ['uses' => 'ProdutoController@index']);
    $router->get('/{user_id}', ['uses' => 'ProdutoController@show']);
    $router->post('', ['uses' => 'ProdutoController@create']);
    $router->put('/{user_id}', ['uses' => 'ProdutoController@edit']);
    $router->delete('/{user_id}', ['uses' => 'ProdutoController@destroy']);
});